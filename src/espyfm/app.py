"""
ESPYFM Flask
------------
This is a simple preloading wrapper around the espyfm library.
"""

import os
import json
import pickle

import numpy as np
from flask import Flask
from flask.logging import create_logger
from minio import Minio

from elasticsearch.exceptions import NotFoundError
from elasticsearch_dsl.connections import connections

from .models import ContentGroup, UserGroup
from .predict import EspyPredict


class EspyApp:
    """
    Flask functionality for EspyFM
    """

    def __init__(self, connection_factory):
        self._predict = None
        self._log = None
        self._no_preload = False

        self._elasticsearch_connection_factory = connection_factory

    def set_no_preload(self):
        self._no_preload = True

    def set_log(self, log):
        self._log = log

    def add_routes(self, app):
        """
        Set the routes for this app
        """

        app.add_url_rule(
            '/users/<user_id>/recommend',
            view_func=self.users_recommend,
            methods=['POST']
        )

        app.add_url_rule(
            '/users/<user_id>/rhs',
            view_func=self.users_rhs,
            methods=['GET']
        )

        app.add_url_rule(
            '/rebuild',
            view_func=self.rebuild,
            methods=['POST']
        )

    def preload(self, mno, mo_bucket):
        """
        Load the model so we can respond quickly
        """

        predict = EspyPredict()

        model_file = 'predict.pkl'

        if self._no_preload:
            self._predict = None
        elif mno and mo_bucket:
            with mno.get_object(mo_bucket, 'predict.pkl') as mno_file:
                state = pickle.load(mno_file)

            predict.load_from_state(state)
            self._predict = predict
        elif os.path.exists(model_file):
            with open(model_file, 'rb') as infile:
                state = pickle.load(infile)

            predict.load_from_state(state)
            self._predict = predict
        else:
            self._log.warning("No existing model found, must rebuild before querying")
            self._predict = None

    @staticmethod
    def wipe():
        """
        Wipe the model and data. USE WITH CARE
        """

        try:
            ContentGroup.wipe_storage()
        except NotFoundError:
            pass

        try:
            UserGroup.wipe_storage()
        except NotFoundError:
            pass

        ContentGroup.init_entries()
        UserGroup.init_entries()

    def rebuild(self):
        """
        Rebuild the model
        """

        conn = self._elasticsearch_connection_factory()

        content = ContentGroup.retrieve()
        users = UserGroup.retrieve()
        ratings = users.get_recommendations()
        categories = users.get_categories() | content.get_categories()

        #ContentGroup.init_entries()

        self.wipe()

        predict = EspyPredict()
        if len(content.keys()) > 0 and len(users.keys()) > 0:
            predict.load_from_data(content, users, categories, ratings, persist=True)

            #RMV: persist
            self._predict = predict

            conn.transport.close()

            return json.dumps({'success': True})

        return json.dumps({'success': False})

    def users_rhs(self, user_id=None):
        """
        Turn a user into a complete RHS for dotting with item vectors
        """

        if user_id == 0:
            user_id = None

        if not user_id:
            raise RuntimeError('Not yet implemented')

        conn = self._elasticsearch_connection_factory()
        user = UserGroup.find(user_id)
        conn.transport.close()

        if self._predict is None:
            self.rebuild()

        rhs = self._predict.get_rhs(user, user_id)

        output = json.dumps([float(r) for r in rhs])
        return output

    def users_recommend(self, user_id):
        """
        Turn a user into a recommendation for dotting with item vectors
        """

        known_items = set()

        # This is redundant de-conversion but helps clarify that recommend is simply a wrapper
        rhs = np.array(json.loads(self.users_rhs(user_id)))

        conn = self._elasticsearch_connection_factory()

        prediction = ContentGroup.retrieve(rhs)
        scores = self._predict.dataset.parse_prediction_group(prediction, known_items)
        conn.transport.close()

        scores = json.dumps([[t, float(r)] for t, r in scores.iteritems()])
        return scores

def create_espy_app(elasticsearch_connection_factory=None):
    """
    Set up the actual Flask app
    """

    if 'ESPYFM_ELASTICSEARCH_HOST' in os.environ:
        elasticsearch_host = os.environ['ESPYFM_ELASTICSEARCH_HOST']
    else:
        elasticsearch_host = 'localhost'

    if elasticsearch_connection_factory is None:
        http_user = os.environ.get('ESPYFM_ELASTICSEARCH_USER', None)
        http_secret_location = os.environ.get('ESPYFM_ELASTICSEARCH_SECRET_LOCATION', None)
        options = {'verify_certs': False}
        if http_user and http_secret_location:
            with open(http_secret_location, 'r') as pass_file:
                http_pass = pass_file.read()
            options['http_auth'] = f'{http_user}:{http_pass}'
        elasticsearch_connection_factory = \
                lambda: connections.create_connection(
                    hosts=[elasticsearch_host],
                    **options
                )

    espy_app = EspyApp(elasticsearch_connection_factory)

    return espy_app

def create_app(elasticsearch_connection_factory=None, espy_app=None):
    if espy_app is None:
        espy_app = create_espy_app(elasticsearch_connection_factory)

    app = Flask(__name__)
    log = create_logger(app)

    espy_app.set_log(log)

    minio_config = (
        'minio_endpoint',
        'minio_key',
        'minio_secret',
        'minio_bucket'
    )

    if all(k in os.environ for k in minio_config):
        mo_config = {
            'endpoint': os.environ['minio_endpoint'],
            'key': os.environ['minio_key'],
            'secret': os.environ['minio_secret'],
            'region': 'us-east-1',
            'bucket': os.environ['minio_bucket']
        }

        mno = Minio(
            mo_config['endpoint'],
            access_key=mo_config['key'],
            secret_key=mo_config['secret'],
            region=mo_config['region'],
            secure=True
        )
        mo_bucket = mo_config['bucket']
    else:
        mno = None
        mo_bucket = None

    if os.environ.get('ESPYFM_ALWAYS_REBUILD_MODEL', False):
        espy_app.set_no_preload()

    espy_app.add_routes(app)

    espy_app.preload(mno, mo_bucket)

    log.info("Preload done.")

    return app, log
