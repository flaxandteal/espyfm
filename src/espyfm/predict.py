# pylint: disable=too-many-arguments,protected-access

"""
EspyFM: LightFM wrapper to perform recommendation search with Elasticsearch
"""

import os
import sys
import pickle

from elasticsearch_dsl.connections import connections

from .models import User, ContentGroup, UserGroup
from .ml import EspyModel
from .dataset import Dataset


NUM_THREADS = 1

class EspyPredict:
    """
    Use the LightFM library to run recommendation predictions.
    """

    dataset = None
    model = None

    def load_from_data(self, content, users, categories, ratings, persist=False):
        """
        Pull in data and build the model, ready for prediction
        """

        model, dataset = self.build_model(content, users, categories, ratings, persist)

        self.model = model
        self.dataset = dataset

    def load_from_state(self, state):
        """
        Hydrate from state
        """

        self.model = EspyModel.create_from_state(state['model'])
        self.dataset = Dataset.create_from_state(state['dataset'])

    def get_prediction_state(self):
        """
        Get saveable state
        """
        return {
            'model': self.model.get_state(),
            'dataset': self.dataset.get_state()
        }

    def get_rhs(self, user=None, user_id=None):
        """
        Return the dottable RHS for a user
        """

        if isinstance(user, User):
            user_features = self.dataset.to_user_features(user.get_categories(), user_id)
        else:
            user_features = None

        return self.model.get_espy_rhs(user_features)

    def run(self, user, known_items=None, use_array=False):
        """
        Predict for a given user (requires loading to be complete)
        """

        right_hand_side = self.get_rhs(user)

        if known_items is None:
            known_items = set()

        if use_array:
            prediction = ContentGroup.search_array(right_hand_side)
            scores = self.dataset.parse_prediction_array(prediction, known_items)
        else:
            prediction = ContentGroup.retrieve(right_hand_side)
            scores = self.dataset.parse_prediction_group(prediction, known_items)

        return scores

    @staticmethod
    def build_model(content, users, categories, ratings, persist=False):
        """
        Creates an ML model from the data passed
        """

        dataset = Dataset.create_dataset(users, content, categories, ratings)

        item_features, user_features = dataset.build_feature_mappings(content, users)

        if isinstance(content, ContentGroup):
            content._features = item_features
            content_group = content
        else:
            content_group = ContentGroup(content, item_features)

        if isinstance(users, UserGroup):
            users._features = user_features
            users_group = users
        else:
            users_group = UserGroup(users, user_features)

        model = EspyModel.create_lightfm_model(
            dataset,
            user_features,
            item_features,
            NUM_THREADS
        )

        content_group.set_embeddings(model.get_item_embeddings())
        content_group.set_biases(model.get_item_biases())
        content_group.build_affine_columns()
        users_group.set_embeddings(model.get_user_embeddings())
        users_group.set_biases(model.get_user_biases())
        users_group.build_affine_columns()

        if persist:
            content_group.persist()
            users_group.persist()

        return model, dataset


def load(dislikes, cache=None):
    """
    Find or build the data model
    """

    if cache and os.path.exists(cache):
        with open(cache, 'rb') as infile:
            content, users, categories, ratings = pickle.load(infile)
    else:
        # raise NotImplementedError(dislikes)
        # FIXME: retrieve from sample JSON
        from .sample import nidirect
        content, users, categories, ratings, _ = nidirect.get_data(dislikes)
        if cache:
             with open(cache, 'wb') as outfile:
                 pickle.dump((content, users, categories, ratings), outfile)
    return content, users, categories, ratings

def initialize(elasticsearch_server, wipe_storage=False):
    """
    Set up and wipe an Elasticsearch server
    """
    connections.create_connection(hosts=[elasticsearch_server])

    if wipe_storage:
        ContentGroup.wipe_storage()
        UserGroup.wipe_storage()

    ContentGroup.init_entries()
    UserGroup.init_entries()

def run(persist, do_predict, elasticsearch_server=None, full_load=False):
    """
    Run an example prediction
    """

    # create the mappings in elasticsearch
    predict = EspyPredict()

    model_file = 'predict.pkl'

    if full_load or not os.path.exists(model_file):
        content, users, categories, ratings = load(
            dislikes=False,
            cache='x.pkl'
        )

        if persist:
            initialize(elasticsearch_server)

        predict.load_from_data(content, users, categories, ratings, persist=persist)

        if model_file:
            with open(model_file, 'wb') as outfile:
                pickle.dump(predict.get_prediction_state(), outfile)

        if do_predict:
            user = User({'m'})
            known_items = set() # set(content.keys())
            scores = predict.run(user, known_items, use_array=True)
            print(scores)
    else:
        if predict:
            connections.create_connection(hosts=[elasticsearch_server])

        with open(model_file, 'rb') as infile:
            state = pickle.load(infile)
        predict.load_from_state(state)


if __name__ == "__main__":
    PERSIST = '--no-persist' not in sys.argv
    DO_PREDICT = '--no-predict' not in sys.argv

    if 'ESPYFM_ELASTICSEARCH_HOST' in os.environ:
        ELASTICSEARCH_HOST = os.environ['ESPYFM_ELASTICSEARCH_HOST']
    else:
        ELASTICSEARCH_HOST = 'localhost'

    run(PERSIST, DO_PREDICT, ELASTICSEARCH_HOST)
