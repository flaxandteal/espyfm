"""
ESPYFM: Run core functions
"""

import os
import click
from espyfm import predict

@click.group()
@click.option('--elasticsearch-server', '-e', default=None)
@click.pass_context
def cli(context, elasticsearch_server):
    """
    General initialization
    """
    if elasticsearch_server is None:
        if 'ESPYFM_ELASTICSEARCH_HOST' in os.environ:
            elasticsearch_server = os.environ['ESPYFM_ELASTICSEARCH_HOST']
        else:
            elasticsearch_server = 'localhost'

    context.obj = {
        'elasticsearch_server': elasticsearch_server
    }

@cli.command()
@click.option('--wipe/--no-wipe', '-w', default=False, help="Wipe indices and recreate")
@click.pass_context
def initialize(context, wipe):
    """
    Set up the Elasticsearch server, wiping if necessary
    """
    print('Initializing...')

    elasticsearch_server = context.obj['elasticsearch_server']

    predict.initialize(
        elasticsearch_server=elasticsearch_server,
        wipe_storage=wipe
    )

@cli.command()
@click.pass_context
def run(context):
    """
    Set up the Elasticsearch server, wiping if necessary
    """
    print('Initializing...')

    elasticsearch_server = context.obj['elasticsearch_server']

    predict.run(
        persist=True,
        do_predict=True,
        elasticsearch_server=elasticsearch_server,
        full_load=True
    )
