"""
ESPYFM Sample: set up sample data
"""

import json
import click
from espyfm import predict
from espyfm.sample import nidirect as sample_nidirect

@click.group()
@click.pass_context
def cli(context):
    """
    General initialization
    """
    context.obj = {
        'output-file': 'sample.json',
    }

@cli.command()
@click.pass_context
def nidirect(context):
    """
    Add content to the model
    """

    content, users, categories, ratings, _ = sample_nidirect.get_data(False)

    pdct = predict.EspyPredict()
    pdct.load_from_data(content, users, categories, ratings)

    users_array = [
        [
            user.get_lightfm_id(),
            {
                content[cidx].get_lightfm_id(): int(score)
                for cidx, score in ratings[idx].items()
            }
            if idx in ratings else {}
        ]
        for idx, user in users.items()
    ]

    items = [
        ctnt.to_dict() for ctnt in content.values()
    ]

    users = [
        user.to_dict() for user in users.values()
    ]

    with open(context.obj['output-file'], 'w') as fobj:
        json.dump({
            'users': users,
            'items': items,
            'recommendations': users_array
        }, fobj)
