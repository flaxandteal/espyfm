"""
Wrapping LightFM's Dataset
"""

# pylint: disable=protected-access

from math import log
import pandas as pd
import numpy as np
import scipy.sparse as sp
from lightfm.data import Dataset as LightFMDataset

class Dataset:
    """
    Wrapper for LightFM's Dataset
    """

    def __init__(self):
        self._dataset = LightFMDataset()
        self.interactions = None
        self.weights = None
        self.user_feature_mapping = None
        self.item_feature_mapping = None
        self.user_id_mapping_rev = None
        self.item_id_mapping_rev = None

    @staticmethod
    def parse_prediction_group(prediction, known_items):
        """
        Turn a prediction group in to a series of scored items
        """

        # https://github.com/lior-k/fast-elasticsearch-vector-scoring
        # "Because scores ... must be non-negative ...
        #  We convert the dot product score ...
        #  (changed dot product) = e^(original dot product)..."
        titles, prediction = zip(*[
            [idx, log(content._score)]
            for idx, content in prediction.items()
        ])
        scores = pd.Series(
            prediction,
            index=titles
        )
        scores = scores[~scores.index.isin(known_items)]

        scores = scores.sort_values(ascending=False)

        return scores

    def parse_prediction_array(self, prediction, known_items):
        """
        Turn a prediction in numpy form into a series of scored items
        """

        content_ids = self.item_id_mapping_rev

        lightfm_ids, prediction = zip(*prediction)
        scores = pd.Series(
            prediction,
            index=[content_ids[x] for x in lightfm_ids]
        )
        scores = scores[~scores.index.isin(known_items)]

        scores = scores.sort_values(ascending=False)

        return scores

    @classmethod
    def create_from_state(cls, state):
        """
        Build from the state
        """

        dataset = cls()
        dataset.user_feature_mapping = state['user_feature_mapping']
        return dataset

    def get_state(self):
        """
        Build essential state
        """
        return {
            'user_feature_mapping': self.user_feature_mapping
        }

    def to_user_features(self, categories, user_id=None):
        """
        Translate an iterable of categories into a user feature matrix
        """

        user_features = list(categories)
        if user_id is not None and user_id in self.user_feature_mapping:
            user_features.append(user_id)

        col = np.array([self.get_user_feature_mapping(c) for c in user_features])
        row = np.array([0] * len(col))
        data = np.array([1] * len(col))

        _, max_feature = self.get_user_feature_mapping_range()
        e_user_features = sp.coo_matrix((data, (row, col)), shape=(1, 1 + max_feature))
        return e_user_features

    def fit(self, users, items, item_features=None, user_features=None):
        """
        Fit the data
        """

        if not item_features:
            item_features = []

        if not user_features:
            user_features = []

        result = self._dataset.fit(
            users,
            items,
            item_features=item_features,
            user_features=user_features
        )
        self.user_id_mapping_rev = {v: k for k, v in self._dataset._user_id_mapping.items()}
        self.item_id_mapping_rev = {v: k for k, v in self._dataset._item_id_mapping.items()}
        return result

    def build_feature_mappings(self, content, users):
        """
        Create the feature mapping for items and users
        """

        item_features_list = [(t, c.get_categories()) for t, c in content.items()]
        item_features = self._dataset.build_item_features(item_features_list)

        for item in item_features_list:
            mapping = self._dataset._item_id_mapping[item[0]]
            content[item[0]].set_lightfm_id(mapping)

        user_features_list = [(t, c.get_categories()) for t, c in users.items()]
        user_features = self._dataset.build_user_features(user_features_list)

        for user in user_features_list:
            mapping = self._dataset._user_id_mapping[user[0]]
            users[user[0]].set_lightfm_id(mapping)

        return item_features, user_features

    def get_item_count(self):
        """
        Get a count of total items
        """

        return len(self._dataset._item_id_mapping)

    def get_user_row(self, user_id):
        """
        Get a count of users
        """

        return self._dataset._user_id_mapping[user_id]

    def get_user_feature_mapping_range(self):
        """
        Get limits of feature IDs
        """

        vals = self.user_feature_mapping.values()
        return (min(vals), max(vals))

    def get_user_feature_mapping(self, feature):
        """
        Get the user mapping for a feature
        """
        return self.user_feature_mapping[feature]

    def build_interactions(self, interactions):
        """
        Build the interactions matrix
        """

        #interactions = [(inter[0], str(inter[1]), inter[2]) for inter in interactions]
        return self._dataset.build_interactions(interactions)

    @classmethod
    def create_dataset(cls, users, contents, categories, ratings):
        """
        Create a LightFM dataset and wrap it
        """

        dataset = cls()
        dataset.fit(
            sorted(users.keys()),
            sorted(contents.keys()),
            item_features=sorted(list(categories)),
            user_features=sorted(list(categories))
        )

        rating_list = [
            (user, content, ratings[user][content])
            for user in ratings if user in ratings
            for content in ratings[user]
            if content in ratings[user]
        ]

        dataset.user_feature_mapping = dataset._dataset._user_feature_mapping
        dataset.item_feature_mapping = dataset._dataset._item_feature_mapping

        interactions, weights = dataset.build_interactions(rating_list)
        for row, weight in enumerate(weights.data):
            if weight <= 0:
                interactions.data[row] *= -1
                weights.data[row] *= -1

        dataset.interactions = interactions
        dataset.weights = weights

        return dataset
