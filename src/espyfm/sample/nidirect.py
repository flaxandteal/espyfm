"""
Generate fake user data for algorithm testing, using NIDirect web analytics as a model.
"""

# pylint: disable=too-many-branches,too-many-locals

import re
import os
from collections import Counter
import numpy as np
import pandas as pd

from ..models import User, Content

TOTAL_AUTO_USERS = 40
NIDIRECT_DATA = os.path.join(
    os.path.dirname(__file__),
    './data/analytics-all-web-site-pageviews-report-20190701-20190930.csv'
)
TEST_DATA = os.path.join(os.path.dirname(__file__), './data/nidirect_test.csv')

def find_all_categories(content, users):
    """
    Pull all the categories for content and user together
    """

    categories = set()
    for cont in content.values():
        categories |= cont.get_categories()
    for user in users.values():
        categories |= user.get_categories()
    return categories

def get_data(dislikes):
    """
    Create the data structures
    """

    nidirect_df = generate_nidirect_df()

    example_correlations, strong_implications = get_distribution_requirements()

    content = {}
    # content = get_sample_content()

    nidirect_content, content = generate_content_from_nidirect(
        nidirect_df,
        example_correlations,
        content
    )

    users = get_sample_users()

    auto_users = get_auto_users()

    ratings = {}
    # ratings = get_sample_ratings()

    ratings = create_nidirect_sample_ratings(
        nidirect_df,
        auto_users,
        nidirect_content,
        strong_implications,
        example_correlations,
        ratings,
        dislikes
    )

    nidirect_df = compute_spread_by_article(ratings, auto_users, nidirect_df)

    users.update(auto_users)
    categories = find_all_categories(content, users)

    nidirect_df.to_csv(TEST_DATA)

    return content, users, categories, ratings, nidirect_df


def generate_nidirect_df():
    """
    Find pages that may be relevant to human pain.
    """

    nidirect_df = pd.read_csv(NIDIRECT_DATA)
    nidirect_df = nidirect_df.drop('Date', axis=1)
    nidirect_df['Page Title'] = nidirect_df['Page Title'].str.replace(r'( \| nidirect)', '')
    nidirect_df = nidirect_df[~nidirect_df['Avg. Time on Page'].str.contains('<')]
    nidirect_df['Avg. Time on Page'] = pd.to_timedelta(
        nidirect_df['Avg. Time on Page']
    ).dt.total_seconds()
    nidirect_df = nidirect_df.groupby('Page Title').mean()
    nidirect_df['Test Popularity'] = nidirect_df['Unique Pageviews'] * \
        nidirect_df['Avg. Time on Page']
    nidirect_df = nidirect_df.sort_values(by='Test Popularity', ascending=False)
    nidirect_df = nidirect_df[
        nidirect_df.index.str.contains('([Pp]ain|algia)') &
        ~nidirect_df.index.str.contains(
            '(children|dog|rabbit|cat|horse|budgeting|vehicle)',
            flags=re.IGNORECASE
        )
    ]

    return nidirect_df

def get_distribution_requirements():
    """
    Set up some probabilistic relationships
    """

    # Numbers are age groups
    example_correlations = [
        (
            ('neurological',),
            (
                'Trigeminal neuralgia',
                'Complex regional pain syndrome (CRPS)',
                'Sore or painful tongue'
            )
        ),
        (
            ('abdominal', 'f'),
            (
                'Stomach ache and abdominal pain',
                'Vulvodynia (vulval pain)',
                'Period pain',
                'Breast pain'
            )
        ),
        (
            ('neckandback',),
            ('Tailbone pain (coccydynia)', 'Neck pain and stiff neck', 'Back pain', 'Shoulder pain')
        ),
        (
            ('joint',),
            (
                'Shoulder pain',
                'Knee pain',
                'Food pain',
                'Hip pain in adults',
                'Hand pain',
                'Joint pain',
                'Heel pain',
                'Metatarsalgia',
                'Polymyalgia rheumatica (PMR)'
            )
        ),
        (
            ('m',),
            ('Priapism (painful erections)')
        ),
    ]

    # ~ is 'not'. It is acknowledged that these
    # correlations are not without important counterexamples,
    # but that they are as strong correlations as we can
    # reasonably infer for numerical testing -- they do
    # NOT inform the actual model or real-world results.
    strong_implications = {
        'Vulvodynia (vulval pain)': {'gender': '~m'},
        'Period pain': {'gender': '~m'},
        'Priapism (painful erections)': {'gender': '~f'},
    }

    return example_correlations, strong_implications

def get_sample_content():
    """
    Create manual sample content
    """

    content = {
        'Top Ten Neurological Pain Tips #1': {
            'medium': 'tip',
            'source': 'PHA',
            #'length': 1,
            'categories': {'neurological'}
        },
        'Top Ten Neurological Pain Tips #2': {
            'medium': 'tip',
            'source': 'PHA',
            #'length': 1,
            'categories': {'neurological'}
        },
        'Top Ten Neurological Pain Tips #3': {
            'medium': 'tip',
            'source': 'PHA',
            #'length': 1,
            'categories': {'neurological'}
        },
        'Pain-Free Cooking': {
            'medium': 'tip',
            'source': 'NHS',
            #'length': 3,
            'categories': {'cooking'}
        },
        'How to Cook with Neurological Pain': {
            'medium': 'video',
            'source': 'NHS',
            #'length': 2,
            'categories': {'cooking', 'neurological'}
        },
        'Managing Abdominal Pain': {
            'medium': 'video',
            'source': 'NHS',
            #'length': 2,
            'categories': {'abdominal'}
        }
    }
    return Content.from_dict(content)

def generate_content_from_nidirect(nidirect_df, example_correlations, content):
    """
    Create content from the NIDirect data
    """

    nidirect_content = {
        title: {} for title in nidirect_df.index
    }
    example_correlations_by_content = {}
    for cats, contents in example_correlations:
        for ex_content in contents:
            if ex_content not in example_correlations_by_content:
                example_correlations_by_content[ex_content] = set()
            example_correlations_by_content[ex_content] |= set(cats)

    for title, attr in nidirect_content.items():
        attr['medium'] = 'text'
        attr['source'] = 'NIDirect'
        attr['length'] = np.random.choice(["1", "2", "3"])

        if title in example_correlations_by_content:
            attr['categories'] = example_correlations_by_content[title]
        else:
            attr['categories'] = set()

    nidirect_content = Content.from_dict(nidirect_content)
    content.update(nidirect_content)
    return nidirect_content, content

def get_sample_users():
    """
    Create some manual sample users
    """

    users = User.from_dict({
        'Ash': {
            'agegroup': '1',
            'mostwantcat': {'cooking'},
            'affect': {'neurological'},
            'lgd': 'Belfast',
            'gender': 'o'
        },
        'Bei': {
            'agegroup': '2',
            'mostwantcat': {'walking'},
            'affect': {'neurological'},
            'lgd': 'Belfast',
            'gender': 'f'
        },
        'Cal': {
            'agegroup': '2',
            'mostwantcat': {'cooking'},
            'affect': {'joint'},
            'lgd': 'Mid Ulster',
            'gender': 'm'
        },
        'Dee': {
            'agegroup': '3',
            'mostwantcat': {'walking'},
            'affect': {'abdominal'},
            'lgd': 'Mid Ulster',
            'gender': 'f'
        },
        'E': {
            'agegroup': '3',
            'mostwantcat': {'cooking'},
            'affect': {'neurological'},
            'lgd': 'Mid Ulster',
            'gender': 'f'
        }
    })

    return users

def get_auto_users():
    """
    Create auto-generated users based on a useful breakdown
    """

    auto_users = {'Z%d' % i: {} for i in range(TOTAL_AUTO_USERS)}
    for _, (_, attr) in enumerate(auto_users.items()):
        attr['gender'] = np.random.choice(['f', 'm', 'o'], p=(0.45, 0.45, 0.1))
        attr['agegroup'] = str(np.random.choice(["1", "2", "3"]))
        attr['mostwantcat'] = set(np.random.choice([
            'cooking', 'driving', 'walking', 'sleeping', 'running', 'swimming'
        ], int(np.random.choice([1, 2, 3], p=(0.8, 0.15, 0.05)))))
        attr['affect'] = set([np.random.choice([
            'neurological', 'abdominal', 'neckandback', 'joint'
        ])])
        attr['lgd'] = np.random.choice([
            'Belfast',
            'Mid Ulster',
            'Antrim & Newtownabbey',
            'Armagh, Banbridge, Craigavon',
            'Fermanagh & Omagh'
        ])

    return User.from_dict(auto_users)

def get_sample_ratings():
    """
    Create manual sample ratings
    """

    ratings = {
        'Ash': {
            'Top Ten Neurological Pain Tips #1': 1,
            'Top Ten Neurological Pain Tips #2': 1,
            'Top Ten Neurological Pain Tips #3': -1,
            'Pain-Free Cooking': 1
        },
        'Bei': {
            'Top Ten Neurological Pain Tips #1': 1,
            'Pain-Free Cooking': -1,
            'How to Cook with Neurological Pain': 1
        },
        'Cal': {
            'Top Ten Neurological Pain Tips #3': 1,
            'Pain-Free Cooking': 1,
            'How to Cook with Neurological Pain': 1
        },
        'Dee': {
            'Top Ten Neurological Pain Tips #3': -1,
            'Pain-Free Cooking': -1,
            'How to Cook with Neurological Pain': 1,
            'Managing Abdominal Pain': 1
        }
    }

    return ratings

def create_nidirect_sample_ratings(nidirect_df, auto_users, nidirect_content,
                                   strong_implications, example_correlations, ratings, dislikes):
    """
    Generate ratings probabilistically from NIDirect data
    """

    max_popularity = nidirect_df['Test Popularity'].max()

    for auto_user, user_attr in auto_users.items():
        for nidirect_item, cont_attr in nidirect_content.items():
            user_cats = user_attr.get_categories()
            cont_cats = cont_attr.get_categories()

            # Make sure, for example, priapism+female doesn't turn up in test data
            if nidirect_item in strong_implications:
                valid = True
                for _, impl in strong_implications[nidirect_item].items():
                    if impl.startswith('~'):
                        valid &= impl[1:] not in user_cats
                    else:
                        valid &= impl in user_cats
                if not valid:
                    if auto_user not in ratings:
                        ratings[auto_user] = {}

                    if dislikes:
                        ratings[auto_user][nidirect_item] = -1
                    continue

            if user_cats & cont_cats:
                # Odds of a rating given category overlap
                probability = 0.1 * (1 - 0.5 * (1 / len(user_cats & cont_cats)))
            else:
                # Odds of a rating given no category overlap
                probability = 0.01
            for cats, titles in example_correlations:
                if user_cats & set(cats) and nidirect_item in titles:
                    probability *= 10

            popularity = nidirect_df.loc[nidirect_item, 'Test Popularity']
            probability = 0.6 * probability + 0.4 * (probability * popularity / max_popularity)

            result = np.random.choice(
                [0, 1, -1 if dislikes else 1],
                p=(
                    1 - probability,
                    probability * (0.7 + 0.3 * popularity / max_popularity),
                    probability * 0.3 * (1 - popularity / max_popularity)
                )
            )

            if result != 0:
                if auto_user not in ratings:
                    ratings[auto_user] = {}

                ratings[auto_user][nidirect_item] = result
            elif auto_user in ratings and nidirect_item in ratings[auto_user]:
                del ratings[auto_user][nidirect_item]

    return ratings

def compute_spread_by_article(ratings, auto_users, nidirect_df):
    """
    Look at each article and see where it's (fake) approval/disapproval comes from
    """
    def test_info(row):
        row['Liked by'] = len([
            u for u, it in ratings.items()
            if row.name in it and it[row.name] == 1
        ])
        row['Disliked by'] = len([
            u for u, it in ratings.items()
            if row.name in it and it[row.name] == -1
        ])

        def user_cats(user_attr):
            """
            Show the categories for this user
            """
            return user_attr.get_categories()

        row['Raters'] = dict(Counter(sum([
            list(user_cats(auto_users[u])) for u, it in ratings.items() if row.name in it
        ], [])))
        row['PRaters'] = dict(Counter(sum([
            list(user_cats(auto_users[u]))
            for u, it in ratings.items() if row.name in it and it[row.name] > 0
        ], [])))
        return row
    nidirect_df = nidirect_df.apply(test_info, axis=1)
    return nidirect_df
