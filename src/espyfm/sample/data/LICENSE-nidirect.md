LICENSE for analytics-all-web-site-pageviews-report-20190701-20190930.csv

This dataset is based on that released by Digital Shared Services within the Department of Finance (NI) [1], under the Open Government License [2]. No warranty or representation of accuracy is provided here - this is included purely for test purposes.

[1] https://www.opendatani.gov.uk/dataset/ni-direct-website-data
[2] http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/
