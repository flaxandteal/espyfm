"""
Core linkage from EspyFM to LightFM
"""

import numpy as np
from lightfm import LightFM

LIGHTFM_COMPONENTS = 5


class EspyModel:
    """
    Wrapper for LightFM model
    """

    def __init__(self, model=None):
        self._model = model
        self._user_embeddings = None
        self._item_embeddings = None
        self._user_biases = None
        self._item_biases = None

    def set_vectors(self, user_embeddings, user_biases, item_embeddings, item_biases):
        """
        Override all vectors - should not commonly be used
        """
        self._user_embeddings = user_embeddings
        self._item_embeddings = item_embeddings
        self._user_biases = user_biases
        self._item_biases = item_biases

    def get_item_embeddings(self):
        """
        Get item biases
        """
        return self._item_embeddings

    def get_user_embeddings(self):
        """
        Get item biases
        """
        return self._user_embeddings

    def get_item_biases(self):
        """
        Get item biases
        """
        return self._item_biases

    def get_user_biases(self):
        """
        Get item biases
        """
        return self._user_biases

    def get_state(self):
        """
        Build summary of essential state
        """
        return {
            'user_embeddings': self._model.user_embeddings,
            'item_embeddings': self._model.item_embeddings,
            'user_biases': self._model.user_biases,
            'item_biases': self._model.item_biases,
        }

    def get_espy_rhs(self, e_user_features):
        """
        Get the RHS for item dotting for a user.
        """
        rhs = e_user_features.dot(self.get_user_embeddings())

        # No bias for an undefined user, but 1 to ensure
        # item biases are captured on dotting
        rhs = np.append(rhs, [1, 0])
        return rhs

    @classmethod
    def create_from_state(cls, state):
        """
        Rehydrate from a state
        """

        model = cls()

        model.set_vectors(
            state['user_embeddings'],
            state['user_biases'],
            state['item_embeddings'],
            state['item_biases']
        )

        return model

    @classmethod
    def create_lightfm_model(cls, dataset, user_features, item_features, num_threads):
        """
        Build the LightFM model and return a wrapped instance
        """

        model = cls(LightFM(no_components=LIGHTFM_COMPONENTS, loss='logistic'))
        model.fit(
            dataset.interactions,
            sample_weight=dataset.weights,
            user_features=user_features,
            item_features=item_features,
            epochs=1000,
            num_threads=num_threads
        )
        return model

    def fit(self, interactions, sample_weight, user_features, item_features, epochs, num_threads):
        """
        Fit the model
        """

        self._model.fit(
            interactions,
            sample_weight=sample_weight,
            user_features=user_features,
            item_features=item_features,
            epochs=epochs,
            num_threads=num_threads
        )

        self._user_embeddings = self._model.user_embeddings
        self._item_embeddings = self._model.item_embeddings
        self._user_biases = self._model.user_biases
        self._item_biases = self._model.item_biases
