# pylint: disable=missing-function-docstring, protected-access
"""
Model Classes for the EspyFM search.
"""

from math import log
#from datetime import datetime
import numpy as np
from elasticsearch_dsl import Q, function, connections
from elasticsearch_dsl.query import Exists
from elasticsearch.helpers import bulk
from .es import Item as ESItem, ItemModel as ESItemModel, encode_array
from .es import User as ESUser, UserModel as ESUserModel

MAX_SEARCH_COUNT = 500

class Group:
    """
    Group of Items for bulk persisting, search and management
    """

    _entity_class = None
    _identifier_field = None

    def __init__(self, entities, features=None):
        """
        Group of entity.
        Item features are only required if creating vectors, i.e. persisting items
        """

        self._entities = entities
        self._features = features
        self._biases = None
        self._embeddings = None

    def __getitem__(self, *args, **kwargs):
        """
        Find the relevant entity item
        """
        return self._entities.__getitem__(*args, **kwargs)

    @classmethod
    def from_dict(cls, dicts):
        entity = cls._entity_class.from_dict(dicts)
        return cls(entity)

    @classmethod
    def wipe_storage(cls):
        """
        Reset the index in ElasticSearch
        """

        cls._entity_class.es_model_class.wipe_index()

    @classmethod
    def get_model_search(cls):
        raise NotImplementedError()

    @classmethod
    def get_search(cls):
        raise NotImplementedError()

    @classmethod
    def get_all(cls):
        """
        Retrieve all entities
        """

        search = cls.get_search()
        # search = search[0:search.count()]
        result = []
        for ent in search.scan():
            ent._identifier = str(ent.meta.id)
            result.append(ent)

        entities = {
            getattr(entry, cls._identifier_field):
                cls._entity_class.from_entry(entry) for entry in result
        }

        return entities

    @classmethod
    def search(cls, vector):
        """
        Search for a vector in Elasticsearch and return Content models
        """
        models = cls.search_raw(vector)
        result = cls.fetch_by_models(models)

        content = {
            getattr(entry, cls._identifier_field):
                cls._entity_class.from_entry(entry) for entry in result
        }

        return content

    @classmethod
    def search_raw(cls, vector):
        """
        Search for a vector in Elasticsearch
        """
        function_score_query = Q(
            'function_score',
            functions=[
                function.SF(
                    'script_score',
                    script={
                        'source': 'binary_vector_score',
                        'lang': 'knn',
                        'params': {
                            'cosine': False,
                            'field': 'embedding_vector',
                            'vector': vector.tolist()
                        }
                    }
                ),
            ],
            boost_mode='replace'
        )

        search = cls.get_model_search()
        search = search.query(Exists(field='embedding_vector'))
        results = []
        for i, ent in enumerate(search.query(function_score_query).sort()):
            ent._identifier = str(ent.meta.id)
            if i > MAX_SEARCH_COUNT:
                break
            results.append(ent)

        return results

    @classmethod
    def fetch_by_models(cls, results):
        search = cls.get_search()
        search = search.filter(
            'ids',
            values=[ent.original_id for ent in results]
        )
        scores = {ent.original_id: ent.meta.score for ent in results}
        results = []
        for i, ent in enumerate(search.sort()):
            ent._identifier = str(ent.meta.id)
            ent._score = scores[ent.meta.id]
            if i > MAX_SEARCH_COUNT:
                break
            results.append(ent)

        return results

    @classmethod
    def search_array(cls, vector):
        """
        Search by vector and return a numpy array
        """
        results = cls.search_raw(vector)

        return np.array([[row.espyfm_lightfm_id, log(row.meta.score)] for row in results])

    @classmethod
    def find(cls, idx):
        """
        Search by ID and return a new result
        """
        # TODO check if this should be Content, not BlogPost

        result = cls._entity_class.es_class.get(idx)

        entry = cls._entity_class.from_entry(result)

        return entry

    @classmethod
    def retrieve(cls, vector=None):
        """
        Search by vector and return a new Group of results
        """
        # TODO check if this should be Content, not BlogPost
        if vector is not None:
            result = cls.search(vector)
        else:
            result = cls.get_all()

        results = cls(result)
        return results

    def items(self):
        """
        Yield the items of the content dictionary
        """
        yield from self._entities.items()

    def keys(self):
        """
        Return the keys of the content dictionary
        """
        return self._entities.keys()

    @property
    def content(self):
        """
        Get the dict of content
        """
        return self._entities

    @property
    def features(self):
        """
        Get the array of item features
        """
        return self._features

    def set_embeddings(self, embeddings):
        """
        Set LightFM's item biases
        """
        self._embeddings = embeddings

    def set_biases(self, biases):
        """
        Set LightFM's item biases
        """
        self._biases = biases

    @classmethod
    def init_entries(cls):
        """
        Initialize the Elasticsearch entries
        """
        cls._entity_class.es_model_class.init()

    def persist(self):
        """
        Save the current content group in Elasticsearch
        """

        visitor = []
        for entity in self._entities.values():
            entity.generate_entry()
            entity.append_model_for_op(visitor)

        # CODEREF https://github.com/elastic/elasticsearch-dsl-py/issues/403
        def _upsert(model):
            upsert_doc = model.to_dict(True)
            upsert_doc.update({
                '_op_type': 'update',
                'doc': upsert_doc['_source'],
                'doc_as_upsert': True
            })
            del upsert_doc['_source']
            return upsert_doc

        bulk_docs = [_upsert(model) for model in visitor]
        bulk(connections.get_connection(), bulk_docs)
        self._entity_class.es_model_class.refresh_index()

class Content:
    """
    Individual item of content
    """

    es_class = ESItem
    es_model_class = ESItemModel

    def __init__(self, name, categories, **kwargs):
        """
        Set up a basic content item
        """

        self._id = None
        self._affine_column = None
        self._name = name
        self._article = None
        self._article_model = None
        self._props = {
            'categories': categories
        }
        self._props.update(kwargs)
        self._score = None

    def append_model_for_op(self, visitor):
        visitor.append(self._article_model)

    def set_lightfm_id(self, lid):
        """
        Set the LightFM ID
        """
        self._id = lid

    def get_lightfm_id(self):
        """
        Get the LightFM ID
        """
        return self._id

    def set_affine_column(self, afcol):
        """
        Set the dottable column
        """
        self._affine_column = afcol

    def get_affine_column(self):
        """
        Get the dottable column
        """
        return self._affine_column

    def get_categories(self):
        """
        Get all categories associated with this item
        """
        return self._props['categories']

    @classmethod
    def from_entry(cls, entry):
        """
        Create a Content item from a BlogPost
        """

        try:
            categories = entry.categories
        except AttributeError:
            categories = []

        content = cls(
            entry.title,
            set(categories),
        )
        content._article = entry
        if hasattr(entry, '_score'):
            content._score = entry._score

        return content

    @classmethod
    def from_dict(cls, dicts):
        """
        Create a Content item from a dictionary of properties
        """
        content = {}
        for name, values in dicts.items():
            content[name] = cls(name, **values)
        return content

    def generate_entry(self):
        """
        Generate an Elasticsearch entry for this item
        """

        # create and save and article
        if self._article_model:
            article_model = self._article_model
        else:
            article_model = self.es_model_class(
                meta={'id': self.get_lightfm_id()}
            )

        if self._article:
            article_model.original_id = self._article.meta.id
        article_model.espyfm_lightfm_id = self.get_lightfm_id()
        article_model.embedding_vector = encode_array(self.get_affine_column())

        self._article_model = article_model

    def to_dict(self):
        """
        Create a dictionary form of this entry
        """

        dct = {
            'id' : self._id,
            'title': self._name
        }
        dct.update(self._props)
        dct['categories'] = list(dct['categories'])
        return dct

    def to_entry_dict(self):
        """
        Create a dictionary form of this entry
        """

        return {
            'title': self._name,
            'categories': list(self._props['categories'])
        }

    def persist_entry(self):
        """
        Save the Elasticsearch entry
        """

        self._article_model.save()

class User:
    """
    User model
    """

    es_class = ESUser
    es_model_class = ESUserModel

    def __init__(self, categories, name=None, **kwargs):
        self._id = None
        self._name = name
        self._props = {
            'categories': categories
        }
        self._props.update(kwargs)
        self._affine_column = None
        self._user = None
        self._user_model = None

    def append_model_for_op(self, visitor):
        visitor.append(self._user_model)

    def set_lightfm_id(self, lid):
        """
        Set the LightFM ID
        """
        self._id = lid

    def get_lightfm_id(self):
        """
        Get the LightFM ID
        """
        return self._id

    def get_categories(self):
        """
        Get an amalgamated set of categories over all fields
        """

        user = self._props
        return user['categories']

    @classmethod
    def from_dict(cls, dicts):
        """
        Create a user from a dict of properties
        """

        users = {}
        for name, values in dicts.items():
            categories = set()
            for _, entry in values.items():
                if not isinstance(entry, set):
                    entry = {entry}
                categories |= entry
            users[name] = cls(categories)
        return users

    def set_affine_column(self, afcol):
        """
        Set the dottable column
        """
        self._affine_column = afcol

    def get_affine_column(self):
        """
        Get the dottable column
        """
        return self._affine_column

    @classmethod
    def from_entry(cls, entry):
        """
        Create a User item from a User ES entity
        """

        try:
            categories = entry.categories
        except AttributeError:
            categories = []

        user = cls(set(categories))
        user._user = entry

        return user

    def generate_entry(self):
        """
        Generate an Elasticsearch entry for this item
        """

        # create and save and user
        if self._user_model:
            user_model = self._user_model
        else:
            user_model = self.es_model_class(
                meta={'id': self.get_lightfm_id()}
            )
        if self._user:
            user_model.original_id = self._user.meta.id
        user_model.espyfm_lightfm_id = self.get_lightfm_id()
        user_model.embedding_vector = encode_array(self.get_affine_column())

        self._user_model = user_model

    def to_dict(self):
        """
        Create a dictionary form of this entry
        """

        dct = {
            'id' : self._id,
            'name': self._name
        }
        dct.update(self._props)
        dct['categories'] = list(dct['categories'])
        return dct

    def to_entry_dict(self):
        """
        Create a dictionary form of this entry
        """

        return {
            'categories': list(self.get_categories())
        }

    def persist_entry(self):
        """
        Save the Elasticsearch entry
        """

        self._user_model.save()

class UserGroup(Group):
    """
    Group of Content Items for bulk persisting, search and management
    """

    _entity_class = User
    _identifier_field = '_identifier'

    def get_recommendations(self):
        return {
            idx: {str(i): 1 for i in u._user.recommendations}
            for idx, u in self.items()
        }

    def get_categories(self):
        return set().union(*[set(u._user.categories) for _, u in self.items()])

    def build_affine_columns(self):
        """
        Create dottable vectors for each user
        """

        def affine_column(user_features, user_bias):
            # EuEc + Bc + Bu
            vector = list(user_features[0]) + [1, user_bias[0]]
            user_f = np.array(vector)
            return user_f

        n_users = len(self._entities)
        for user in self._entities.values():
            lightfm_id = user.get_lightfm_id()
            user_bias = self.features.dot(self._biases).reshape((n_users, 1))
            # RMV: Should this bias be zero or one perhaps?
            afcol = affine_column(
                self.features[lightfm_id].dot(self._embeddings),
                user_bias[lightfm_id]
            ).transpose()
            user.set_affine_column(afcol)

    @classmethod
    def get_model_search(cls):
        return ESUserModel.search()

    @classmethod
    def get_search(cls):
        return ESUser.search()

class ContentGroup(Group):
    """
    Group of Content Items for bulk persisting, search and management
    """

    _entity_class = Content
    _identifier_field = '_identifier'

    def build_affine_columns(self):
        """
        Create dottable vectors for each content item
        """

        def affine_column(item_features, item_bias):
            # EuEc + Bc + Bu
            vector = list(item_features[0]) + [item_bias[0], 1]
            item_f = np.array(vector)
            return item_f

        n_items = len(self._entities)
        for item in self._entities.values():
            lightfm_id = item.get_lightfm_id()
            item_bias = self.features.dot(self._biases).reshape((n_items, 1))
            afcol = affine_column(
                self.features[lightfm_id].dot(self._embeddings),
                item_bias[lightfm_id]
            ).transpose()
            item.set_affine_column(afcol)

    def get_categories(self):
        return set().union(*[set(c._article.categories) for _, c in self.items()])

    @classmethod
    def get_model_search(cls):
        return ESItemModel.search()

    @classmethod
    def get_search(cls):
        return ESItem.search()
