#!/usr/bin/env python
'''
Amendments Copyright (c) 2019 Phil Weir
Licensed under the MIT license.
Copyright (c) 2015 Jesse Peterson
Licensed under the MIT license. See the included LICENSE.txt file for details.
Copyright (c) Alex Ellis 2017. All rights reserved.
Licensed under the MIT license. See LICENSE file in the project root for full license information.
'''

#pylint: disable=invalid-name

import logging
from flask import request
from . import app as appm


def setup():
    """
    Set up function
    """

    current_app, log = appm.create_app()  # Creating an app object
    log.addHandler(logging.StreamHandler()) # Adding handler/logger to app
    log.setLevel(logging.INFO) # Set level of logger to INFO
    return current_app # return app object


app = setup() # call setup method on app object

@app.before_request
def fix_transfer_encoding():
    """
    Sets the "wsgi.input_terminated" environment flag, thus enabling
    Werkzeug to pass chunked requests as streams.  The gunicorn server
    should set this, but it's not yet been implemented.
    """

    transfer_encoding = request.headers.get("Transfer-Encoding", None)
    if transfer_encoding == u"chunked":
        request.environ["wsgi.input_terminated"] = True

if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0')
