# pylint: disable=too-many-arguments

"""
Core linkage from EspyFM to Elasticsearch
"""

# pylint: disable=protected-access,too-few-public-methods,arguments-differ

import base64
import numpy as np
from elasticsearch_dsl import Document, Integer, Keyword, Text, Field

DFLOAT32 = np.dtype('>f4')
SHARDS = 2

class Binary(Field):
    """
    Type of Elasticsearch field for binary data
    """
    name = 'binary'

def decode_float_list(base64_string):
    """
    Turn a base64 string into a vector
    """

    byt = base64.b64decode(base64_string)
    return np.frombuffer(byt, dtype=DFLOAT32).tolist()

def encode_array(arr):
    """
    Turn a vector into a base64 string
    """

    base64_str = base64.b64encode(np.array(arr).astype(DFLOAT32)).decode("utf-8")
    return base64_str



class EspyDocument(Document):
    """
    Elasticsearch Document for an Espy object
    """

    @classmethod
    def refresh_index(cls):
        """
        Refresh the Elasticsearch index
        """
        cls._index.refresh()

    @classmethod
    def wipe_index(cls):
        """
        Delete and clear the Elasticsearch index
        """
        cls._index.delete()
        # cls._index.flush()

    def save(self, **kwargs):
        """
        Persist this item
        """
        return super().save(**kwargs)

class Item(EspyDocument):
    """
    Elasticsearch model for content
    """

    title = Text()
    categories = Keyword(multi=True)

    espyfm_lightfm_id = Integer()
    embedding_vector = Binary(doc_values=True)

    class Index:
        """
        Elasticsearch Index for Content
        """

        name = 'item_index'
        settings = {
        }

class User(EspyDocument):
    """
    Elasticsearch model for user
    """

    categories = Keyword(multi=True)
    recommendations = Keyword(multi=True)

    espyfm_lightfm_id = Integer()

    class Index:
        """
        Elasticsearch Index for Users
        """

        name = 'user_index'
        settings = {
        }

class ItemModel(EspyDocument):
    """
    Elasticsearch model for the item ML model
    """

    original_id = Keyword()
    espyfm_lightfm_id = Integer()
    embedding_vector = Binary(doc_values=True)

    class Index:
        """
        Elasticsearch Index for Users
        """

        name = 'item_model_index'
        settings = {
        }

class UserModel(EspyDocument):
    """
    Elasticsearch model for user ML model
    """

    original_id = Keyword()
    espyfm_lightfm_id = Integer()
    embedding_vector = Binary(doc_values=True)

    class Index:
        """
        Elasticsearch Index for Users
        """

        name = 'user_model_index'
        settings = {
        }
