import pytest
import math
from elasticmock import fake_elasticsearch
import numpy as np
import json
from espyfm.app import create_app
from espyfm.es import decode_float_list, encode_array

def create_recommend_request(client, user_id):
    return client.post(f'/users/{user_id}/recommend')

def create_rebuild_request(client):
    return client.post(f'/rebuild')

def create_rhs_request(client, user_id):
    return client.get(f'/users/{user_id}/rhs')

def dot_hit_and_vector(hit, vec):
    embedding_vector = decode_float_list(hit['_source']['embedding_vector'])

    # https://github.com/lior-k/fast-elasticsearch-vector-scoring
    # "Because scores ... must be non-negative ...
    #  We convert the dot product score ...
    #  (changed dot product) = e^(original dot product)..."

    result = math.exp(np.dot(np.array(embedding_vector), np.array(vec)))
    return result

@pytest.fixture
def items(elasticsearch_conn):
    items = [
        {
            '_id': 1,
            'title': 'ABC',
            'categories': ['sciatica']
        },
        {
            '_id': 2,
            'title': 'DRM',
            'categories': ['abdominal']
        },
        {
            '_id': 3,
            'title': 'XYZ',
            'categories': ['sciatica']
        },
    ]
    item_dict = {
        item['title']:
        elasticsearch_conn.index('item_index', item)
        for item in items
    }
    elasticsearch_conn.index('item_model_index', {
        'original_id': 1,
        'espy_lightfm_id': 1,
        'embedding_vector': encode_array([1, 0, 0, 1, 0, 0, 1])
    })
    id_dict = {item.get('_id'): item for item in item_dict.values()}
    item_dict.update(id_dict)
    return item_dict

@pytest.fixture
def users(elasticsearch_conn, items):
    user = elasticsearch_conn.index('user_index', {
        '_id': 1,
        'name': '123',
        'recommendations': [items['ABC'].get('_id')],
        'categories': ['sciatica']
    })
    elasticsearch_conn.index('user_model_index', {
        'original_id': 1,
        'espy_lightfm_id': 10,
        'embedding_vector': encode_array([1, 0, 0, 1, 0, 0, 1])
    })
    return {
        '123': user,
    }

def test_get_recommendation_for_user(client, elasticsearch_conn, users, items):
    elasticsearch_conn.swap_fake_query(
        'bool',
        lambda condition: 'must' in condition and 'exists' in condition['must'][0],
        'bool',
        {'match_all': {}}
    )
    elasticsearch_conn.swap_fake_query(
        'bool',
        lambda condition: 'filter' in condition and 'ids' in condition['filter'][0],
        'bool',
        {'match_all': {}}
    )
    rv = create_rhs_request(client, users['123'].get('_id'))
    result = json.loads(rv.data)
    elasticsearch_conn.set_scorer('item_model_index', lambda h: dot_hit_and_vector(h, result))
    rv = create_recommend_request(client, users['123'].get('_id'))

    assert rv.status_code == 200

    result = json.loads(rv.data)
    scores = {
        elasticsearch_conn.get('item_index', item[0])['_source']['title']: item[1]
        for item in result
    }

    # Make sure that ABC is at least an order of magnitude better match than DRM
    assert round(math.log(scores['ABC'])) > round(math.log(abs(scores['DRM'])))
    assert scores['ABC'] > scores['XYZ'] * 1.2
    assert round(math.log(scores['XYZ'])) > round(math.log(abs(scores['DRM'])))

def test_rebuild(client, elasticsearch_conn, users, items):
    user_dict = elasticsearch_conn.get('user_index', users['123'].get('_id'))
    rv = create_rebuild_request(client)

    assert rv.status_code == 200

def test_get_rhs_for_user(client, elasticsearch_conn, users, items):
    rv = create_rhs_request(client, users['123'].get('_id'))

    assert rv.status_code == 200

def test_get_same_rhs_for_user_twice(client, elasticsearch_conn, users, items):
    rv = create_rhs_request(client, users['123'].get('_id'))
    result = json.loads(rv.data)

    rv_2 = create_rhs_request(client, users['123'].get('_id'))
    result_2 = json.loads(rv_2.data)

    assert rv.status_code == 200
    assert rv_2.status_code == 200
    assert result == result_2
