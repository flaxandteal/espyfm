import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import json
import pytest
import logging
from unittest.mock import MagicMock
from espyfm.app import create_app, create_espy_app
from elasticsearch_dsl.connections import add_connection, connections
from elasticmock.fake_elasticsearch import Elasticsearch, FakeQueryCondition, \
    QueryType, FakeIndicesClient, extract_ignore_as_iterable, \
    NotFoundError, unicode, FakeClusterClient, get_random_id, get_random_scroll_id
from elasticmock.utilities.decorator import for_all_methods
from elasticmock.behaviour.server_failure import server_failure
from elasticsearch.client.utils import query_params

class EspyFakeQueryCondition(FakeQueryCondition):
    def _evaluate_for_compound_query_type(self, document):
        return_val = False
        if isinstance(self.condition, dict):
            for query_type, sub_query in self.condition.items():
                return_val = EspyFakeQueryCondition(
                    QueryType.get_query_type(query_type),
                    sub_query
                ).evaluate(document)
                if not return_val:
                    return False
        elif isinstance(self.condition, list):
            for sub_condition in self.condition:
                for sub_condition_key in sub_condition:
                    return_val = EspyFakeQueryCondition(
                        QueryType.get_query_type(sub_condition_key),
                        sub_condition[sub_condition_key]
                    ).evaluate(document)
                    if not return_val:
                        return False

        return return_val

    def _evaluate_for_query_type(self, document):
        if self.type == QueryType.MATCH_ALL:
            return True
        else:
            return super()._evaluate_for_query_type(document)

@for_all_methods([server_failure])
class FakeElasticsearch(Elasticsearch):
    _documents_dict = None

    def __init__(self, hosts=None, transport_class=None, **kwargs):
        self._documents_dict = {}
        self.__scrolls = {}

    @property
    def indices(self):
        return FakeIndicesClient(self)

    @property
    def cluster(self):
        return FakeClusterClient(self)

    @property
    def _FakeElasticsearch__documents_dict(self):
        return self._documents_dict

    @query_params()
    def ping(self, params=None, headers=None):
        return True

    @query_params()
    def info(self, params=None, headers=None):
        return {
            'status': 200,
            'cluster_name': 'elasticmock',
            'version':
                {
                    'lucene_version': '4.10.4',
                    'build_hash': '00f95f4ffca6de89d68b7ccaf80d148f1f70e4d4',
                    'number': '1.7.5',
                    'build_timestamp': '2016-02-02T09:55:30Z',
                    'build_snapshot': False
                },
            'name': 'Nightwatch',
            'tagline': 'You Know, for Search'
        }

    @query_params('consistency',
                  'op_type',
                  'parent',
                  'refresh',
                  'replication',
                  'routing',
                  'timeout',
                  'timestamp',
                  'ttl',
                  'version',
                  'version_type')
    def index(self, index, body, doc_type='_doc', id=None, params=None, headers=None):
        if index not in self._documents_dict:
            self._documents_dict[index] = list()
        
        version = 1

        if id is None:
            id = get_random_id()
        elif self.exists(index, doc_type, id, params=params):
            doc = self.get(index, id, doc_type, params=params)
            version = doc['_version'] + 1
            self.delete(index, doc_type, id)

        self._documents_dict[index].append({
            '_type': doc_type,
            '_id': id,
            '_source': body,
            '_index': index,
            '_version': version
        })

        return {
            '_type': doc_type,
            '_id': id,
            'created': True,
            '_version': version,
            '_index': index
        }

    @query_params('consistency', 'op_type', 'parent', 'refresh', 'replication',
                  'routing', 'timeout', 'timestamp', 'ttl', 'version', 'version_type')
    def bulk(self,  body, index=None, doc_type=None, params=None, headers=None):
        version = 1
        items = []

        for line in body.splitlines():
            if len(line.strip()) > 0:
                line = json.loads(line)

                if 'index' in line:
                    index = line['index']['_index']
                    doc_type = line['index']['_type']

                    if index not in self._documents_dict:
                        self._documents_dict[index] = list()
                else:
                    document_id = get_random_id()

                    self._documents_dict[index].append({
                        '_type': doc_type,
                        '_id': document_id,
                        '_source': line,
                        '_index': index,
                        '_version': version
                    })

                    items.append({'index': {
                        '_type': doc_type,
                        '_id': document_id,
                        '_index': index,
                        '_version': version,
                        'result': 'created',
                        'status': 201
                    }})

        return {
            'errors': False,
            'items': items
        }

    @query_params('parent', 'preference', 'realtime', 'refresh', 'routing')
    def exists(self, index, doc_type, id, params=None, headers=None):
        result = False
        if index in self._documents_dict:
            for document in self._documents_dict[index]:
                if document.get('_id') == id and document.get('_type') == doc_type:
                    result = True
                    break
        return result

    @query_params('_source', '_source_exclude', '_source_include', 'fields',
                  'parent', 'preference', 'realtime', 'refresh', 'routing', 'version',
                  'version_type')
    def get(self, index, id, doc_type='_all', params=None, headers=None):
        ignore = extract_ignore_as_iterable(params)
        result = None

        if index in self._documents_dict:
            for document in self._documents_dict[index]:
                if document.get('_id') == id:
                    if doc_type == '_all':
                        result = document
                        break
                    else:
                        if document.get('_type') == doc_type:
                            result = document
                            break

        if result:
            result['found'] = True
            return result
        elif params and 404 in ignore:
            return {'found': False}
        else:
            error_data = {
                '_index': index,
                '_type': doc_type,
                '_id': id,
                'found': False
            }
            raise NotFoundError(404, json.dumps(error_data))


    @query_params('_source', '_source_exclude', '_source_include', 'parent',
                  'preference', 'realtime', 'refresh', 'routing', 'version',
                  'version_type')
    def get_source(self, index, doc_type, id, params=None, headers=None):
        document = self.get(index=index, doc_type=doc_type, id=id, params=params)
        return document.get('_source')

    @query_params('_source', '_source_exclude', '_source_include',
                  'allow_no_indices', 'analyze_wildcard', 'analyzer', 'default_operator',
                  'df', 'expand_wildcards', 'explain', 'fielddata_fields', 'fields',
                  'from_', 'ignore_unavailable', 'lenient', 'lowercase_expanded_terms',
                  'preference', 'q', 'request_cache', 'routing', 'scroll', 'search_type',
                  'size', 'sort', 'stats', 'suggest_field', 'suggest_mode',
                  'suggest_size', 'suggest_text', 'terminate_after', 'timeout',
                  'track_scores', 'version')
    def count(self, index=None, doc_type=None, body=None, params=None, headers=None):
        searchable_indexes = self._normalize_index_to_list(index)

        i = 0
        for searchable_index in searchable_indexes:
            for document in self._documents_dict[searchable_index]:
                if doc_type is not None and document.get('_type') != doc_type:
                    continue
                i += 1
        result = {
            'count': i,
            '_shards': {
                'successful': 1,
                'failed': 0,
                'total': 1
            }
        }

        return result

    def _get_fake_query_condition(self, query_type_str, condition):
        return FakeQueryCondition(QueryType.get_query_type(query_type_str), condition)

    @query_params('_source', '_source_exclude', '_source_include',
                  'allow_no_indices', 'analyze_wildcard', 'analyzer', 'default_operator',
                  'df', 'expand_wildcards', 'explain', 'fielddata_fields', 'fields',
                  'from_', 'ignore_unavailable', 'lenient', 'lowercase_expanded_terms',
                  'preference', 'q', 'request_cache', 'routing', 'scroll', 'search_type',
                  'size', 'sort', 'stats', 'suggest_field', 'suggest_mode',
                  'suggest_size', 'suggest_text', 'terminate_after', 'timeout',
                  'track_scores', 'version')
    def search(self, index=None, doc_type=None, body=None, params=None, headers=None):
        searchable_indexes = self._normalize_index_to_list(index)

        matches = []
        conditions = []

        if body and 'query' in body:
            query = body['query']
            for query_type_str, condition in query.items():
                conditions.append(self._get_fake_query_condition(query_type_str, condition))
        for searchable_index in searchable_indexes:
            for document in self._documents_dict[searchable_index]:
                if doc_type:
                    if isinstance(doc_type, list) and document.get('_type') not in doc_type:
                        continue
                    if isinstance(doc_type, str) and document.get('_type') != doc_type:
                        continue
                if conditions:
                    for condition in conditions:
                        if condition.evaluate(document):
                            matches.append(document)
                            break
                else:
                    matches.append(document)

        result = {
            'hits': {
                'total': len(matches),
                'max_score': 1.0
            },
            '_shards': {
                # Simulate indexes with 1 shard each
                'successful': len(searchable_indexes),
                'failed': 0,
                'total': len(searchable_indexes)
            },
            'took': 1,
            'timed_out': False
        }

        hits = []
        for match in matches:
            match['_score'] = 1.0
            hits.append(match)

        # build aggregations
        if body is not None and 'aggs' in body:
            aggregations = {}

            for aggregation, definition in body['aggs'].items():
                aggregations[aggregation] = {
                    "doc_count_error_upper_bound": 0,
                    "sum_other_doc_count": 0,
                    "buckets": []
                }

            if aggregations:
                result['aggregations'] = aggregations

        if 'scroll' in params:
            result['_scroll_id'] = str(get_random_scroll_id())
            params['size'] = int(params.get('size') if 'size' in params else 10)
            params['from'] = int(params.get('from') + params.get('size') if 'from' in params else 0)
            self.__scrolls[result.get('_scroll_id')] = {
                'index': index,
                'doc_type': doc_type,
                'body': body,
                'params': params
            }
            hits = hits[params.get('from'):params.get('from') + params.get('size')]
        
        result['hits']['hits'] = hits
        
        return result

    @query_params('scroll')
    def scroll(self, scroll_id, params=None, headers=None):
        scroll = self.__scrolls.pop(scroll_id)
        result = self.search(
            index=scroll.get('index'),
            doc_type=scroll.get('doc_type'),
            body=scroll.get('body'),
            params=scroll.get('params')
        )
        return result
    
    @query_params('consistency', 'parent', 'refresh', 'replication', 'routing',
                  'timeout', 'version', 'version_type')
    def delete(self, index, doc_type, id, params=None, headers=None):

        found = False
        ignore = extract_ignore_as_iterable(params)

        if index in self._documents_dict:
            for document in self._documents_dict[index]:
                if document.get('_type') == doc_type and document.get('_id') == id:
                    found = True
                    self._documents_dict[index].remove(document)
                    break

        result_dict = {
            'found': found,
            '_index': index,
            '_type': doc_type,
            '_id': id,
            '_version': 1,
        }

        if found:
            return result_dict
        elif params and 404 in ignore:
            return {'found': False}
        else:
            raise NotFoundError(404, json.dumps(result_dict))

    @query_params('allow_no_indices', 'expand_wildcards', 'ignore_unavailable',
                  'preference', 'routing')
    def suggest(self, body, index=None, params=None, headers=None):
        if index is not None and index not in self._documents_dict:
            raise NotFoundError(404, 'IndexMissingException[[{0}] missing]'.format(index))

        result_dict = {}
        for key, value in body.items():
            text = value.get('text')
            suggestion = int(text) + 1 if isinstance(text, int) else '{0}_suggestion'.format(text)
            result_dict[key] = [
                {
                    'text': text,
                    'length': 1,
                    'options': [
                        {
                            'text': suggestion,
                            'freq': 1,
                            'score': 1.0
                        }
                    ],
                    'offset': 0
                }
            ]
        return result_dict

    def _normalize_index_to_list(self, index):
        # Ensure to have a list of index
        if index is None:
            searchable_indexes = self._documents_dict.keys()
        elif isinstance(index, str) or isinstance(index, unicode):
            searchable_indexes = [index]
        elif isinstance(index, list):
            searchable_indexes = index
        else:
            # Is it the correct exception to use ?
            raise ValueError("Invalid param 'index'")

        # Check index(es) exists
        for searchable_index in searchable_indexes:
            if searchable_index not in self._documents_dict:
                raise NotFoundError(404, 'IndexMissingException[[{0}] missing]'.format(searchable_index))

        return searchable_indexes

@for_all_methods([server_failure])
class EspyFakeElasticsearch(FakeElasticsearch):
    def __init__(self, *args, **kwargs):
        self.substitutes = {}
        self.scorers = {}
        super().__init__(*args, **kwargs)

    def set_scorer(self, index, scorer=None):
        self.scorers[index] = scorer

    def swap_fake_query(self, query_type_str, match, substitute_query_type_str, substitute_condition):
        if query_type_str not in self.substitutes:
            self.substitutes[query_type_str] = []
        self.substitutes[query_type_str].append((match, substitute_query_type_str, substitute_condition))

    def _get_fake_query_condition(self, query_type_str, condition):
        if query_type_str in self.substitutes:
            for match, substitute_query_type_str, substitute_condition in self.substitutes[query_type_str]:
                if match(condition):
                    condition = substitute_condition
                    query_type_str = substitute_query_type_str
        return EspyFakeQueryCondition(QueryType.get_query_type(query_type_str), condition)

    @query_params('consistency',
                  'op_type',
                  'parent',
                  'refresh',
                  'replication',
                  'routing',
                  'timeout',
                  'timestamp',
                  'ttl',
                  'version',
                  'version_type')
    def index(self, index, body, doc_type='_doc', id=None, params=None, headers=None):
        if self.exists(index, doc_type, id, params=params):
            result = 'updated'
        else:
            result = 'created'
        meta = super().index(index=index, body=body, doc_type=doc_type, id=id, params=params, headers=headers)
        meta['result'] = result
        return meta

    @query_params("rest_total_hits_as_int", "scroll")
    def scroll(self, body=None, scroll_id=None, params=None, headers={}):
        if not scroll_id:
            scroll_id = body['scroll_id']
        return super().scroll(scroll_id=scroll_id, params=params)

    @query_params('_source', '_source_exclude', '_source_include',
                  'allow_no_indices', 'analyze_wildcard', 'analyzer', 'default_operator',
                  'df', 'expand_wildcards', 'explain', 'fielddata_fields', 'fields',
                  'from_', 'ignore_unavailable', 'lenient', 'lowercase_expanded_terms',
                  'preference', 'q', 'request_cache', 'routing', 'scroll', 'search_type',
                  'size', 'sort', 'stats', 'suggest_field', 'suggest_mode',
                  'suggest_size', 'suggest_text', 'terminate_after', 'timeout',
                  'track_scores', 'version')
    def search(self, index=None, doc_type=None, body=None, params=None, headers=None):
        result = super().search(index=index, doc_type=doc_type, body=body, params=params, headers=headers)
        # https://github.com/vrcmarcos/elasticmock/issues/50
        if '_shards' in result:
            result['_shards']['skipped'] = 0
        joint_index = ','.join(index) if isinstance(index, list) else index
        if joint_index in self.scorers and self.scorers[joint_index] is not None:
            for hit in result['hits']['hits']:
                hit['_score'] = self.scorers[joint_index](hit)
        return result

    @query_params('consistency', 'op_type', 'parent', 'refresh', 'replication',
                  'routing', 'timeout', 'timestamp', 'ttl', 'version', 'version_type')
    def bulk(self,  body, index=None, doc_type=None, params=None, headers=None):
        version = 1
        items = []

        update_next_line = False
        for line in body.splitlines():
            if len(line.strip()) > 0:
                line = json.loads(line)

                if update_next_line:
                    update_next_line = False
                    if line_index in self._documents_dict:
                        found = False
                        for document in self._documents_dict[line_index]:
                            if document.get('_id') == line_id:
                                found = True
                                doc = self._documents_dict[line_index]['_source']
                                doc.update(line['doc'])
                        if not found:
                            if 'doc_as_upsert' in line and line['doc_as_upsert']:
                                doc = line['doc']
                            else:
                                doc = line['upsert']
                            document_id = get_random_id()
                            self._documents_dict[line_index].append({
                                '_type': doc_type,
                                '_id': document_id,
                                '_source': doc,
                                '_index': line_index,
                                '_version': version
                            })
                elif 'index' in line:
                    index = line['index']['_index']
                    doc_type = line['index']['_type']

                    if index not in self._documents_dict:
                        self._documents_dict[index] = list()
                elif 'update' in line:
                    line_index = line['update']['_index']
                    line_id = line['update']['_id']
                    update_next_line = True
                else:
                    document_id = get_random_id()

                    self._documents_dict[index].append({
                        '_type': doc_type,
                        '_id': document_id,
                        '_source': line,
                        '_index': index,
                        '_version': version
                    })

                    items.append({'index': {
                        '_type': doc_type,
                        '_id': document_id,
                        '_index': index,
                        '_version': version,
                        'result': 'created',
                        'status': 201
                    }})

        return {
            'errors': False,
            'items': items
        }

@pytest.fixture
def elasticsearch_conn():
    conn = EspyFakeElasticsearch()
    add_connection('default', conn)
    conn.transport = MagicMock()
    conn.transport.serializer = json
    return conn

@pytest.fixture
def app(elasticsearch_conn):
    espy_app = create_espy_app(lambda: elasticsearch_conn)
    espy_app.set_no_preload()
    app, log = create_app(lambda: elasticsearch_conn, espy_app=espy_app)
    app.test_log = log
    return app


@pytest.fixture
def client(app):
    with app.test_client() as client:
        yield client
