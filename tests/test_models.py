from espyfm.models import User

def test_make_user():
    """
    Ensure a user can be created
    """

    name = 'Any'
    agegroup = '1'
    mostwantcat = 'abdominal'
    lgd = 'Belfast'
    gender = 'o'
    affect = 'abdominal'

    user = User(name, agegroup=agegroup, mostwantcat=mostwantcat, affect=affect, lgd=lgd, gender=gender)

    assert user
