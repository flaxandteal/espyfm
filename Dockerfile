FROM python:3.8

RUN apt update && \
  apt install -y git gcc cython3 python3-dev python3-numpy python3-scipy python3-pandas

RUN mkdir /app
ADD requirements.txt /app

WORKDIR /app

RUN pip3 install -r requirements.txt

ADD LICENSE.txt /app
ADD gunicorn_config.py /app
ADD README.md /app
ADD setup.py /app
ADD src /app/src

RUN python3 setup.py develop

CMD ["gunicorn", "--config", "./gunicorn_config.py", "espyfm.wsgi:app"]
