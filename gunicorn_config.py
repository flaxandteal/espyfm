import os
port = os.environ['ESPYFM_PORT'] if 'ESPYFM_PORT' in os.environ else '5000'
bind = os.environ['ESPYFM_BIND'] if 'ESPYFM_BIND' in os.environ else "0.0.0.0:%s" % port
workers = 1
timeout = 600
reload = False
