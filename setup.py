from setuptools import setup, find_packages

# Acknowledgements to Lintol for
# MIT licensed setup.py (https://gitlab.com/lintol/doorstep

cmdclass = {}
try:
    from babel.messages import frontend as babel
    cmdclass.update({
        'compile_catalog': babel.compile_catalog,
        'extract_messages': babel.extract_messages,
        'init_catalog': babel.init_catalog,
        'update_catalog': babel.update_catalog,
    })
except ImportError as e:
    pass

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError as e:
    pass

name = 'espyfm'
version = '0.0.1'
setup(
    name='espyfm',
    version=version,
    description='Elasticsearch/LightFM recommendation tool',
    url='https://github.com/flaxandteal/espyfm',
    author='Phil Weir',
    author_email='phil.weir@flaxandteal.co.uk',
    license='MIT',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5'
    ],
    keywords='lightfm recommendation elasticsearch',
    setup_requires=['pytest-runner'],
    extras_require={
        'babel-commands': ['Babel'],
        'sphinx-commands': ['sphinx']
    },
    install_requires=[
        'elasticsearch_dsl',
        'flask',
        'minio',
        'lightfm @ git+https://github.com/lyst/lightfm',
        'gunicorn',
        'scipy',
        'numpy',
        'pandas',
        'Click'
    ],
    include_package_data=True,
    tests_require=[
        'pytest'
    ],
    entry_points='''
        [console_scripts]
        espyfm=espyfm.scripts.espyfm:cli
        espyfm-sample=espyfm.scripts.espyfm_sample:cli
    ''',
    cmdclass=cmdclass,
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version)
        }
    }
)
