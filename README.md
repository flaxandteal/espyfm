# EspyFM Server

## LightFM &amp; Elasticsearch Wrapper

EspyFM is a wrapper around Lyst's [LightFM](https://gitlab.com/lyst/lightfm)
library.

## Running

    docker-compose up
    docker exec -ti espyfm_espyfm_1 espyfm initialize
    curl localhost:5001
